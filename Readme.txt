UCE-Kit (Unbiased Consciential Experiments Kit) Readme
www.uce-kit.org
------------------------------------------------------

This is an Open Sourced Project. It contains hardware designs, software code and instructions of the UCE-Kit.
Research presented at: 1st International Congress of Conscientiology - http://icc.iacworld.org/portfolio/hugo_gonzalez/


------------------------
Creative Commons License
UCE-Kit (Unbiased Consciential Experiments Kit) by Hugo Gonz�lez Castro: https://bitbucket.org/protoidea
is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License: http://creativecommons.org/licenses/by-nc/4.0/
Based on a work at http://www.uce-kit.org/ and https://bitbucket.org/protoidea/uce-kit.

