
//////////////////////////////////

const unsigned long startTime = 300000; // 5 min = 300000 ms
const unsigned long totalTime = 900000; // 15 min = 900000 ms
const boolean bEnciendeLed = true;
const boolean bDebug = false;

const unsigned long minLapse = 30000; // 30 seg = 30000 ms
const unsigned long maxLapse = 300000; // 5 min = 300000 ms
const unsigned long duration = 30000; // 30 seg = 30000 ms

int pinactivo1 = 44; // Infrarrojo
int pinactivo2 = 50; // Electroimán

///////////////////////////////////

void shortbeep()
{
  digitalWrite(13, HIGH);
  delay(50);
  digitalWrite(13, LOW);
}

void beep()
{
  digitalWrite(13, HIGH);
  delay(500);
  digitalWrite(13, LOW);
}

inline int pinvalmando(int nmando)
{
  return 8 - nmando;
}

inline int pinledmando(int nmando)
{
  return 22 - nmando;
}

int valmando(int nmando)
{
  int npin = pinvalmando(nmando);
  int val = analogRead(npin);
  if      ((val > 400) && (val < 600)) return 3; // botón naranja
  else if ((val > 800) && (val < 940)) return 2; // botón verde
  else if ((val > 950) && (val < 1020)) return 1; // botón negro
  return 0;
}

void printresta(long a, long b)
{
  if (a < b)
  {
    Serial.print("-");
    Serial.print(b-a);
  }
  else if (a > b)
  {
    Serial.print("+");
    Serial.print(a-b);
  }
  else
    Serial.print("0");
}  


unsigned long nextTime = startTime;
int isActive = 0;

int periodoactivo()
{
  unsigned long mil = millis();
  if (bDebug)
  {
    Serial.print(nextTime);
    Serial.print(" [;] ");
    Serial.println(mil);
  }
  
  // Nunca activar después del tiempo total:
  if (totalTime < mil)
    return 0;
  
  // Llegó el momento de hacer algo...
  if (nextTime < mil)
  {
    // Si no está activo, lo activamos (la primera vez siempre se sabe)
    if (isActive == 0)
    {
      isActive = 1;
      nextTime = nextTime + duration;
    }
    else // Si está activo, lo desactivamos y planificamos la siguiente activación.
    {
      isActive = 0;
      unsigned long randduration = random(minLapse, maxLapse);
      nextTime = nextTime + randduration;
    }
  }
  
  return isActive;
}
  
////////////////////////////////////////////////////////  

const int nmandos = 8;
unsigned long puntospositivos[nmandos+1];
unsigned long puntosnegativos[nmandos+1];

void setup()
{
  Serial.begin(9600);
  
  // Random seed
  randomSeed(analogRead(15));
  
  // Beep no
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  
  // Led sí
  pinMode(12, OUTPUT);
  digitalWrite(12, HIGH);
  
  // Pin activo
  pinMode(pinactivo1, OUTPUT);
  pinMode(pinactivo2, OUTPUT);
  digitalWrite(pinactivo1, LOW);
  digitalWrite(pinactivo2, LOW);
  
  // Apago LEDs de los mandos
  for (int n=1; n<=nmandos; ++n)
  {
    int pin = pinledmando(n);
    pinMode(pin, OUTPUT);
    digitalWrite(pin, LOW);
    
    // Inicializa puntuaciones
    puntospositivos[n] = 0;
    puntosnegativos[n] = 0;
  }    

  // Tiempo de espera inicial:
  Serial.println("Initial wait...");
  delay(startTime);
  shortbeep();
  Serial.println("Running...");
}

////////////////////////////////////////////////////////


void loop()
{
  // Resumen si se ha terminado:
  unsigned long mil = millis();
  if (bDebug)
  {
    Serial.print(totalTime);
    Serial.print(" ; ");
    Serial.println(mil);
  }
  
  if (totalTime < mil)
  {
    beep();
    
    Serial.println("Results:");
    for (int nmando = 1; nmando <= nmandos; ++nmando)
    {
      Serial.print("Controller ");
      Serial.print(nmando);
      Serial.print(" has: +");
      Serial.print(puntospositivos[nmando]);
      Serial.print(" -");
      Serial.print(puntosnegativos[nmando]);
      Serial.print(" = ");
      printresta(puntospositivos[nmando], puntosnegativos[nmando]);
      Serial.println(" points");
    }
    
    // Apaga Led
    digitalWrite(12, LOW);
    digitalWrite(pinactivo1, LOW);
    digitalWrite(pinactivo1, LOW);
    delay(30000);
  }
 
 
  // Activación aleatoria del pin:
  int valactivo = periodoactivo();
  if (valactivo == 1)
  {
    digitalWrite(pinactivo1, HIGH);
    digitalWrite(pinactivo2, HIGH);
  }
  else
  {
    digitalWrite(pinactivo1, LOW);
    digitalWrite(pinactivo2, LOW);
  }

  if (bDebug)
  {
    Serial.print("Active value: ");
    Serial.println(valactivo);
  }

  
  // Medición otros mandos y comparación:
  for (int nmando = 1; nmando <= nmandos; ++nmando)
  {
    int valmandoN = valmando(nmando);
   
    // Si pulsa algo:
    if (valmandoN != 0)
    {
      int pin = pinledmando(nmando);
  
      if (valmandoN == valactivo) // acierto
      {
        puntospositivos[nmando] += 10;
        
        if (bEnciendeLed)
        {
          digitalWrite(pin, HIGH);
          
          Serial.print("Controller ");
          Serial.print(nmando);
          Serial.println(" gets +10 points");
        }
      }
      else if (valactivo != 0) // dice otro
      {
        puntospositivos[nmando] += 1;
         
        if (bEnciendeLed)
        {
          Serial.print("Controller ");
          Serial.print(nmando);
          Serial.println(" gets +1 point");
        }
      }
      else if (valmandoN == 1) // fallo total: falla estando seguro de acertar
      {
        puntosnegativos[nmando] += 10;
        
        if (bEnciendeLed)
        {
          Serial.print("Controller ");
          Serial.print(nmando);
          Serial.println(" gets -10 points");
        }
      }
      else // fallo estando con dudas
      {
        puntosnegativos[nmando] += 1;
        
        if (bEnciendeLed)
        {
          Serial.print("Controller ");
          Serial.print(nmando);
          Serial.println(" gets -1 points");
        }
      }
     
      // Apaga led:
      digitalWrite(pin, LOW);
    }
 
    if (bDebug)
    {
      Serial.print("Controller ");
      Serial.print(nmando);
      Serial.print(" button: ");
      Serial.println(valmandoN);
    }
    
    delay(1);
  }

}



