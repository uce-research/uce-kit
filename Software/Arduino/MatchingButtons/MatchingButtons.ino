
//////////////////////////////////

const unsigned long totalTime = 600000; // 10 min = 600000 ms
const boolean bEnciendeLed = true;
const boolean bDebug = false;

///////////////////////////////////

void beep()
{
  digitalWrite(13, HIGH);
  delay(500);
  digitalWrite(13, LOW);
}

inline int pinvalmando(int nmando)
{
  return 8 - nmando;
}

inline int pinledmando(int nmando)
{
  return 22 - nmando;
}

int valmando(int nmando)
{
  int npin = pinvalmando(nmando);
  int val = analogRead(npin);
  if      ((val > 400) && (val < 600)) return 3; // botón naranja
  else if ((val > 800) && (val < 940)) return 2; // botón verde
  else if ((val > 950) && (val < 1020)) return 1; // botón negro
  return 0;
}

void printresta(long a, long b)
{
  if (a < b)
  {
    Serial.print("-");
    Serial.print(b-a);
  }
  else if (a > b)
  {
    Serial.print("+");
    Serial.print(a-b);
  }
  else
    Serial.print("0");
}  
  
////////////////////////////////////////////////////////  

const int nmandos = 8;
unsigned long puntospositivos[nmandos+1];
unsigned long puntosnegativos[nmandos+1];

void setup()
{
  Serial.begin(9600);
  
  // Beep no
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
  
  // Led sí
  pinMode(12, OUTPUT);
  digitalWrite(12, HIGH);
  
  // Apago LEDs de los mandos
  for (int n=1; n<=nmandos; ++n)
  {
    int pin = pinledmando(n);
    pinMode(pin, OUTPUT);
    digitalWrite(pin, LOW);
    
    // Inicializa puntuaciones
    puntospositivos[n] = 0;
    puntosnegativos[n] = 0;
  }    

}

////////////////////////////////////////////////////////


void loop()
{
  // Resumen si se ha terminado:
  unsigned long mil = millis();
  if (bDebug)
  {
    Serial.print(totalTime);
    Serial.print(" ; ");
    Serial.println(mil);
  }
  
  if (totalTime < mil)
  {
    beep();
    
    Serial.println("Results:");
    for (int nmando = 1; nmando <= nmandos; ++nmando)
    {
      Serial.print("Controller ");
      Serial.print(nmando);
      Serial.print(" has: +");
      Serial.print(puntospositivos[nmando]);
      Serial.print(" -");
      Serial.print(puntosnegativos[nmando]);
      Serial.print(" = ");
      printresta(puntospositivos[nmando], puntosnegativos[nmando]);
      Serial.println(" points");
    }
    
    // Apaga Led
    digitalWrite(12, LOW);
    delay(30000);
  }
 
  // Medición del mando 1:
  int pinmando1 = pinledmando(1);
  int valmando1 = valmando(1);
  if (bDebug)
  {
    Serial.print("Controller 1 button: ");
    Serial.println(valmando1);
  }

  
  // Medición otros mandos y comparación:
  for (int nmando = 2; nmando <= nmandos; ++nmando)
  {
    int valmandoN = valmando(nmando);
   
    // Si pulsa algo:
    if (valmandoN != 0)
    {
      int pin = pinledmando(nmando);
  
      if (valmandoN == valmando1) // acierto
      {
        puntospositivos[nmando] += 10;
        
        if (bEnciendeLed)
        {
          digitalWrite(pinmando1, HIGH);
          digitalWrite(pin, HIGH);
          
          Serial.print("Controller ");
          Serial.print(nmando);
          Serial.println(" gets +10 points");
        }
      }
      else if (valmando1 != 0) // dice otro
      {
        puntospositivos[nmando] += 1;
         
        if (bEnciendeLed)
        {
          Serial.print("Controller ");
          Serial.print(nmando);
          Serial.println(" gets +1 point");
        }
      }
      else // fallo total
      {
        puntosnegativos[nmando] += 10;
        
        if (bEnciendeLed)
        {
          Serial.print("Controller ");
          Serial.print(nmando);
          Serial.println(" gets -10 points");
        }
      }
     
      // Apaga led:
      digitalWrite(pinmando1, LOW);
      digitalWrite(pin, LOW);
    }
 
    if (bDebug)
    {
      Serial.print("Controller ");
      Serial.print(nmando);
      Serial.print(" button: ");
      Serial.println(valmandoN);
    }
    
    delay(1);
  }

}



