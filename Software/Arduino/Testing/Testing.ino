
#define TEST_PIN_A 44
#define TEST_PIN_B 51


// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin 13 as an output.
  for (int n=TEST_PIN_A; n<TEST_PIN_B; ++n)
    pinMode(n, OUTPUT);
    
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  
  for (int n=TEST_PIN_A; n<TEST_PIN_B; ++n)
    digitalWrite(n, LOW);
    
  digitalWrite(12, LOW);
  digitalWrite(13, LOW);
}

// the loop function runs over and over again forever
void loop() {

  digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)
  for (int n=TEST_PIN_A; n<TEST_PIN_B; ++n)
    digitalWrite(n, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);              // wait for a second

  digitalWrite(12, LOW);    // turn the LED off by making the voltage LOW
  for (int n=TEST_PIN_A; n<TEST_PIN_B; ++n)
    digitalWrite(n, LOW);   // turn the LED on (HIGH is the voltage level)
  delay(1000);              // wait for a second

}


/*
#define TEST_PIN_A 14
#define TEST_PIN_B 21


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  // initialize digital pin 13 as an output.
  for (int n=TEST_PIN_A; n<TEST_PIN_B; ++n)
    pinMode(n, OUTPUT);
    
  pinMode(12, OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  
  for (int nsen=0; nsen<7; ++nsen)
  {
    int sensorValue = analogRead(nsen);
  
    if (sensorValue > 0)
      digitalWrite(nsen+14, HIGH);
    else
      digitalWrite(nsen+14, LOW);
    
    // print out the value you read:
    Serial.println(sensorValue);
    delay(1);        // delay in between reads for stability
  }

}
*/

/*
void setup() {
  // put your setup code here, to run once:
  pinMode(12, OUTPUT);

  // Random seed: not connected pin 15:
  randomSeed(analogRead(15));
  
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:


  digitalWrite(12, LOW);
  int randNumber = random(5000);
  Serial.print("Random number: ");
  Serial.println(randNumber);

  delay(randNumber);
  digitalWrite(12, HIGH);
  delay(1000);

}

*/


